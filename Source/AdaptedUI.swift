//
//  AdaptedUI.swift
//  AdaptedUI
//
//  Copyright (c) 2018 Luis Cárdenas
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

public final class AdaptedUI: NSObject {
    
    private var screenWidth = UIScreen.main.bounds.width
    private var screenHeight = UIScreen.main.bounds.height
    private var baseSize: CGSize!
    
    public static let main: AdaptedUI = AdaptedUI()
    
    
    /// Use this method to set the base size of the mockup. If the mockup is from an iPhone 7 plus and an iPad, you should set
    /// something like this:
    /// ```
    /// let size = UIDevice.current.userInterfaceIdiom == .pad ?
    ///     CGSize(width: 768.0, height: 1024.0) :
    ///     CGSize(width: 375.0, height: 677.0)
    /// AdaptedUI.main.setBase(size: size)
    /// ```
    /// - Parameters:
    ///     - size:         The `size` (width and height) of the mockup screens.
    public func setBase(size: CGSize) {
        baseSize = size
    }
    
    public func adapt(width: CGFloat) -> CGFloat {
        return adapt(size: width, original: baseSize.width, end: min(screenWidth, screenHeight))
    }
    
    public func adapt(height: CGFloat) -> CGFloat {
        return adapt(size: height, original: baseSize.height, end: max(screenWidth, screenHeight))
    }
    
    private func adapt(size: CGFloat, original: CGFloat, end: CGFloat) -> CGFloat {
        return end * size / original
    }
    
}
